
package ru.iteco.taskmanager.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.iteco.taskmanager.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CheckPassword_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "checkPassword");
    private final static QName _CheckPasswordResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "checkPasswordResponse");
    private final static QName _FindAll_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "findAll");
    private final static QName _FindAllResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "findAllResponse");
    private final static QName _FindByLogin_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "findByLogin");
    private final static QName _FindByLoginResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "findByLoginResponse");
    private final static QName _Get_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "get");
    private final static QName _GetById_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "getById");
    private final static QName _GetByIdResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "getByIdResponse");
    private final static QName _GetCurrent_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "getCurrent");
    private final static QName _GetCurrentResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "getCurrentResponse");
    private final static QName _GetResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "getResponse");
    private final static QName _Merge_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "merge");
    private final static QName _MergeDefault_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "mergeDefault");
    private final static QName _MergeDefaultResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "mergeDefaultResponse");
    private final static QName _MergeResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "mergeResponse");
    private final static QName _Set_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "set");
    private final static QName _SetCurrent_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "setCurrent");
    private final static QName _SetCurrentResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "setCurrentResponse");
    private final static QName _SetResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "setResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.iteco.taskmanager.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckPassword }
     * 
     */
    public CheckPassword createCheckPassword() {
        return new CheckPassword();
    }

    /**
     * Create an instance of {@link CheckPasswordResponse }
     * 
     */
    public CheckPasswordResponse createCheckPasswordResponse() {
        return new CheckPasswordResponse();
    }

    /**
     * Create an instance of {@link FindAll }
     * 
     */
    public FindAll createFindAll() {
        return new FindAll();
    }

    /**
     * Create an instance of {@link FindAllResponse }
     * 
     */
    public FindAllResponse createFindAllResponse() {
        return new FindAllResponse();
    }

    /**
     * Create an instance of {@link FindByLogin }
     * 
     */
    public FindByLogin createFindByLogin() {
        return new FindByLogin();
    }

    /**
     * Create an instance of {@link FindByLoginResponse }
     * 
     */
    public FindByLoginResponse createFindByLoginResponse() {
        return new FindByLoginResponse();
    }

    /**
     * Create an instance of {@link Get }
     * 
     */
    public Get createGet() {
        return new Get();
    }

    /**
     * Create an instance of {@link GetById }
     * 
     */
    public GetById createGetById() {
        return new GetById();
    }

    /**
     * Create an instance of {@link GetByIdResponse }
     * 
     */
    public GetByIdResponse createGetByIdResponse() {
        return new GetByIdResponse();
    }

    /**
     * Create an instance of {@link GetCurrent }
     * 
     */
    public GetCurrent createGetCurrent() {
        return new GetCurrent();
    }

    /**
     * Create an instance of {@link GetCurrentResponse }
     * 
     */
    public GetCurrentResponse createGetCurrentResponse() {
        return new GetCurrentResponse();
    }

    /**
     * Create an instance of {@link GetResponse }
     * 
     */
    public GetResponse createGetResponse() {
        return new GetResponse();
    }

    /**
     * Create an instance of {@link Merge }
     * 
     */
    public Merge createMerge() {
        return new Merge();
    }

    /**
     * Create an instance of {@link MergeDefault }
     * 
     */
    public MergeDefault createMergeDefault() {
        return new MergeDefault();
    }

    /**
     * Create an instance of {@link MergeDefaultResponse }
     * 
     */
    public MergeDefaultResponse createMergeDefaultResponse() {
        return new MergeDefaultResponse();
    }

    /**
     * Create an instance of {@link MergeResponse }
     * 
     */
    public MergeResponse createMergeResponse() {
        return new MergeResponse();
    }

    /**
     * Create an instance of {@link Set }
     * 
     */
    public Set createSet() {
        return new Set();
    }

    /**
     * Create an instance of {@link SetCurrent }
     * 
     */
    public SetCurrent createSetCurrent() {
        return new SetCurrent();
    }

    /**
     * Create an instance of {@link SetCurrentResponse }
     * 
     */
    public SetCurrentResponse createSetCurrentResponse() {
        return new SetCurrentResponse();
    }

    /**
     * Create an instance of {@link SetResponse }
     * 
     */
    public SetResponse createSetResponse() {
        return new SetResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link AbstractEntity }
     * 
     */
    public AbstractEntity createAbstractEntity() {
        return new AbstractEntity();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "checkPassword")
    public JAXBElement<CheckPassword> createCheckPassword(CheckPassword value) {
        return new JAXBElement<CheckPassword>(_CheckPassword_QNAME, CheckPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "checkPasswordResponse")
    public JAXBElement<CheckPasswordResponse> createCheckPasswordResponse(CheckPasswordResponse value) {
        return new JAXBElement<CheckPasswordResponse>(_CheckPasswordResponse_QNAME, CheckPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAll }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "findAll")
    public JAXBElement<FindAll> createFindAll(FindAll value) {
        return new JAXBElement<FindAll>(_FindAll_QNAME, FindAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "findAllResponse")
    public JAXBElement<FindAllResponse> createFindAllResponse(FindAllResponse value) {
        return new JAXBElement<FindAllResponse>(_FindAllResponse_QNAME, FindAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "findByLogin")
    public JAXBElement<FindByLogin> createFindByLogin(FindByLogin value) {
        return new JAXBElement<FindByLogin>(_FindByLogin_QNAME, FindByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "findByLoginResponse")
    public JAXBElement<FindByLoginResponse> createFindByLoginResponse(FindByLoginResponse value) {
        return new JAXBElement<FindByLoginResponse>(_FindByLoginResponse_QNAME, FindByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Get }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "get")
    public JAXBElement<Get> createGet(Get value) {
        return new JAXBElement<Get>(_Get_QNAME, Get.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "getById")
    public JAXBElement<GetById> createGetById(GetById value) {
        return new JAXBElement<GetById>(_GetById_QNAME, GetById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "getByIdResponse")
    public JAXBElement<GetByIdResponse> createGetByIdResponse(GetByIdResponse value) {
        return new JAXBElement<GetByIdResponse>(_GetByIdResponse_QNAME, GetByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "getCurrent")
    public JAXBElement<GetCurrent> createGetCurrent(GetCurrent value) {
        return new JAXBElement<GetCurrent>(_GetCurrent_QNAME, GetCurrent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "getCurrentResponse")
    public JAXBElement<GetCurrentResponse> createGetCurrentResponse(GetCurrentResponse value) {
        return new JAXBElement<GetCurrentResponse>(_GetCurrentResponse_QNAME, GetCurrentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "getResponse")
    public JAXBElement<GetResponse> createGetResponse(GetResponse value) {
        return new JAXBElement<GetResponse>(_GetResponse_QNAME, GetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Merge }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "merge")
    public JAXBElement<Merge> createMerge(Merge value) {
        return new JAXBElement<Merge>(_Merge_QNAME, Merge.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeDefault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "mergeDefault")
    public JAXBElement<MergeDefault> createMergeDefault(MergeDefault value) {
        return new JAXBElement<MergeDefault>(_MergeDefault_QNAME, MergeDefault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeDefaultResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "mergeDefaultResponse")
    public JAXBElement<MergeDefaultResponse> createMergeDefaultResponse(MergeDefaultResponse value) {
        return new JAXBElement<MergeDefaultResponse>(_MergeDefaultResponse_QNAME, MergeDefaultResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "mergeResponse")
    public JAXBElement<MergeResponse> createMergeResponse(MergeResponse value) {
        return new JAXBElement<MergeResponse>(_MergeResponse_QNAME, MergeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Set }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "set")
    public JAXBElement<Set> createSet(Set value) {
        return new JAXBElement<Set>(_Set_QNAME, Set.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetCurrent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "setCurrent")
    public JAXBElement<SetCurrent> createSetCurrent(SetCurrent value) {
        return new JAXBElement<SetCurrent>(_SetCurrent_QNAME, SetCurrent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetCurrentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "setCurrentResponse")
    public JAXBElement<SetCurrentResponse> createSetCurrentResponse(SetCurrentResponse value) {
        return new JAXBElement<SetCurrentResponse>(_SetCurrentResponse_QNAME, SetCurrentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "setResponse")
    public JAXBElement<SetResponse> createSetResponse(SetResponse value) {
        return new JAXBElement<SetResponse>(_SetResponse_QNAME, SetResponse.class, null, value);
    }

}
