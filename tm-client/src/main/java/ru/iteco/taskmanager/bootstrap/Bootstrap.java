package ru.iteco.taskmanager.bootstrap;

import java.util.Scanner;
import java.util.Set;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import lombok.Getter;
import lombok.Setter;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.endpoint.DomainEndpointService;
import ru.iteco.taskmanager.endpoint.ProjectEndpointService;
import ru.iteco.taskmanager.endpoint.SessionEndpointService;
import ru.iteco.taskmanager.endpoint.TaskEndpointService;
import ru.iteco.taskmanager.endpoint.UserEndpointService;
import ru.iteco.taskmanager.service.SessionService;
import ru.iteco.taskmanager.service.TerminalService;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

	@NotNull
	private Scanner scanner;
	@NotNull
	private String inputCommand;
	@NotNull
	private final TerminalService terminalService = new TerminalService();
	@NotNull
	private final SessionService sessionService = new SessionService();
	@NotNull
	private final UserEndpointService UserEndpointService = new UserEndpointService();
	@NotNull
	private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
	@NotNull
	private final TaskEndpointService taskEndpointService = new TaskEndpointService();
	@NotNull
	private final DomainEndpointService domainEndpointService = new DomainEndpointService();
	@NotNull
	private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
	@NotNull
    private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.iteco.taskmanager").getSubTypesOf(AbstractCommand.class);
	
	public void init() {
		scanner = new Scanner(System.in);
		try {
			registerCommand(classes);
			terminalService.get("login").execute();
			System.out.println("Enter command (type help for more information)");

			while (true) {
				System.out.print("> ");
				inputCommand = scanner.nextLine();
				if (!terminalService.getCommands().containsKey(inputCommand)) {
					System.out.println("Command doesn't exist");
				} else {
					terminalService.get(inputCommand).execute();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void registerCommand(@Nullable final Set<Class<? extends AbstractCommand>> classes) throws Exception {
        if (classes == null) return;
        for (@Nullable Class clazz : classes) {
            if (clazz == null || !AbstractCommand.class.isAssignableFrom(clazz)) continue;
            @NotNull AbstractCommand command = (AbstractCommand) clazz.newInstance();
            command.setServiceLocator(this);
            registerCommand(command);
        }
    }

    private void registerCommand(@Nullable final AbstractCommand command) {
        if (command == null) return;
        terminalService.put(command.command(), command);
    }	
}
