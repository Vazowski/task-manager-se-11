package ru.iteco.taskmanager.command.serialize.load;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.command.AbstractCommand;

public class DataFromBinCommand extends AbstractCommand {

	@Override
	public @NotNull String command() {
		return "data-from-bin";
	}

	@Override
	public @NotNull String description() {
		return "  -  load data from binary file";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final Session session = serviceLocator.getSessionService().getSession();
		if (session == null) return;
		
		if (serviceLocator.getDomainEndpointService().getDomainEndpointPort().loadData(session) != null) {
			serviceLocator.getSessionService().setSession(null);
            System.out.println("Done");
		}
	}
}
