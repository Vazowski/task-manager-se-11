package ru.iteco.taskmanager.command.system;

import ru.iteco.taskmanager.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

	@Override
	public String command() {
		return "exit";
	}

	@Override
	public String description() {
		return "  -  exit the programm";
	}

	@Override
	public void execute() throws Exception {
		System.exit(0);
	}
}
