package ru.iteco.taskmanager.command.serialize.save;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.command.AbstractCommand;

public class DataToBinCommand extends AbstractCommand {

	@Override
	public @NotNull String command() {
		return "data-to-bin";
	}

	@Override
	public @NotNull String description() {
		return "  -  save data to binary file";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final Session session = serviceLocator.getSessionService().getSession();
		if (session == null) return;
		
		if (serviceLocator.getDomainEndpointService().getDomainEndpointPort().saveData(session) != null) {
			serviceLocator.getSessionService().setSession(null);
            System.out.println("Done");
		}
	}
}
