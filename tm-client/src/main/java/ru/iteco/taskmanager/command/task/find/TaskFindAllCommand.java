package ru.iteco.taskmanager.command.task.find;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.Task;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;

public final class TaskFindAllCommand extends AbstractCommand {
	
	@Override
	public String command() {
		return "task-find-all";
	}

	@Override
	public String description() {
		return "  -  find all task in project";
	}

	@Override
	public void execute() throws Exception {
		@NotNull 
		final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
		@NotNull 
		final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpointService().getProjectEndpointPort();
		@NotNull 
		final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpointService().getTaskEndpointPort();
        @Nullable 
        final Session session = serviceLocator.getSessionService().getSession();
        if (session == null) return;
        @Nullable final User user = userEndpoint.getById(session, session.getUserId());
        if (user == null) return;
		
        System.out.print("Name of project: ");
        @Nullable
		final String inputProjectName = scanner.nextLine();
        @Nullable 
		final Project tempProject = projectEndpoint.findProjectByName(session, inputProjectName);
		if (tempProject == null) {
			System.out.println("Project doesn't exist");
			return;
		}
		
		@Nullable
		final List<Task> taskList = taskEndpoint.findAllByProjectUuid(session, tempProject.getUuid());
		if (taskList == null) throw new Exception("No task exist");
		
		for (int i = 0, j = 1; i < taskList.size(); i++) {
			System.out.println("[Task " + (j++) + "]");
			System.out.println("UUID: " + taskList.get(i).getUuid());
			System.out.println("ProjectUUID: " + taskList.get(i).getProjectUUID());
			System.out.println("Name: " + taskList.get(i).getName());
			System.out.println("Description: " + taskList.get(i).getDescription());
			System.out.println("DateCreated: " + taskList.get(i).getDateCreated());
			System.out.println("DateBegin: " + taskList.get(i).getDateBegin());
			System.out.println("DateEnd: " + taskList.get(i).getDateEnd());
			System.out.println("Status: " + taskList.get(i).getReadinessStatus().toString());
		}
	}
}
