package ru.iteco.taskmanager.command.serialize.load;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.command.AbstractCommand;

public class DataFromJsonJacksonCommand extends AbstractCommand {

	@Override
	public String command() {
		return "jackson-from-json";
	}

	@Override
	public String description() {
		return "  -  load data from json via jackson";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final Session session = serviceLocator.getSessionService().getSession();
		if (session == null) return;
		
		if (serviceLocator.getDomainEndpointService().getDomainEndpointPort().jacksonLoadJson(session) != null) {
			serviceLocator.getSessionService().setSession(null);
            System.out.println("Done");
		}
	}

}
