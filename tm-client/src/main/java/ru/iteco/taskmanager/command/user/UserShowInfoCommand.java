package ru.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;

public final class UserShowInfoCommand extends AbstractCommand {

	@Override
	public String command() {
		return "user-show";
	}

	@Override
	public String description() {
		return "  -  show user info";
	}

	@Override
	public void execute() throws Exception {
		@NotNull 
		final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
        @Nullable 
        final Session session = serviceLocator.getSessionService().getSession();
        if (session == null) return;
        @Nullable final User user = userEndpoint.get(session, session.getUserId());
        if (user == null) return;
		System.out.println("Current login: " + user.getLogin());
		System.out.println("Current role: " + user.getRoleType());
	}
}
