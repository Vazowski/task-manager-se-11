package ru.iteco.taskmanager.command.serialize.save;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.command.AbstractCommand;

public class DataToJsonJaxbCommand extends AbstractCommand {

	@Override
	public String command() {
		return "jaxb-to-json";
	}

	@Override
	public String description() {
		return "  -  save data to json via jaxb";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final Session session = serviceLocator.getSessionService().getSession();
		if (session == null) return;
		
		if (serviceLocator.getDomainEndpointService().getDomainEndpointPort().jaxbSaveJson(session) != null) {
			serviceLocator.getSessionService().setSession(null);
            System.out.println("Done");
		}
	}

}
