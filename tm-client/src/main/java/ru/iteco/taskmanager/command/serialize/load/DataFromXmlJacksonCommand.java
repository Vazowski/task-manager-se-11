package ru.iteco.taskmanager.command.serialize.load;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.command.AbstractCommand;

public class DataFromXmlJacksonCommand extends AbstractCommand {

	@Override
	public @NotNull String command() {
		return "jackson-from-xml";
	}

	@Override
	public @NotNull String description() {
		return "  -  load data from xml via jackson";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final Session session = serviceLocator.getSessionService().getSession();
		if (session == null) return;
		
		if (serviceLocator.getDomainEndpointService().getDomainEndpointPort().jacksonLoadXml(session) != null) {
			serviceLocator.getSessionService().setSession(null);
            System.out.println("Done");
		}
	}

}
