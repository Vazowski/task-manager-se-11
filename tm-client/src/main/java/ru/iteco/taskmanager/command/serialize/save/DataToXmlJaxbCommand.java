package ru.iteco.taskmanager.command.serialize.save;

import java.util.List;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.command.AbstractCommand;

public class DataToXmlJaxbCommand extends AbstractCommand {

	@Override
	public String command() {
		return "jaxb-to-xml";
	}

	@Override
	public String description() {
		return "  -  save data to xml via jaxb";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final Session session = serviceLocator.getSessionService().getSession();
		if (session == null) return;
		
		if (serviceLocator.getDomainEndpointService().getDomainEndpointPort().jaxbSaveXml(session) != null) {
			serviceLocator.getSessionService().setSession(null);
            System.out.println("Done");
		}
	}
}
