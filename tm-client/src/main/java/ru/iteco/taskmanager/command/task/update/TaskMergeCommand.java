package ru.iteco.taskmanager.command.task.update;

import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.Task;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;

public final class TaskMergeCommand extends AbstractCommand {

	@Override
	public String command() {
		return "task-merge";
	}

	@Override
	public String description() {
		return "  -  merge task";
	}

	@Override
	public void execute() throws Exception {
		@NotNull 
		final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
		@NotNull 
		final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpointService().getProjectEndpointPort();
		@NotNull 
		final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpointService().getTaskEndpointPort();
        @Nullable 
        final Session session = serviceLocator.getSessionService().getSession();
        if (session == null) return;
        @Nullable final User user = userEndpoint.getById(session, session.getUserId());
        if (user == null) return;
		
        System.out.print("Name of project: ");
		@NotNull
        final String inputProjectName = scanner.nextLine();
		@Nullable
		final Project tempProject = projectEndpoint.findProjectByName(session, inputProjectName);
		if (tempProject == null) {
			System.out.println("Project doesn't exist");
			return;
		} 
		System.out.print("Name of task: ");
		@NotNull
		final String inputName = scanner.nextLine();
		System.out.print("Description of task: ");
		@NotNull
		final String inputDescription = scanner.nextLine();
		System.out.print("Date of begining task: ");
		@NotNull
		final String dateBegin = scanner.nextLine();
		System.out.print("Date of ending task: ");
		@NotNull
		final String dateEnd = scanner.nextLine();
		@Nullable
		final Task tempTask = taskEndpoint.findByName(session, inputName);
		
		if (tempTask == null) {
			String uuid = UUID.randomUUID().toString();
			taskEndpoint.taskMerge(session, inputName, inputDescription, uuid, tempProject.getUuid(), user.getUuid(), dateBegin, dateEnd);
			System.out.println("Done");
			return;
		}
		
		if (tempTask.getOwnerId() == user.getUuid()) {
			taskEndpoint.taskMerge(session, inputName, inputDescription, tempTask.getUuid(), tempProject.getUuid(), user.getUuid(), dateBegin, dateEnd);
			System.out.println("Done");
			return;
		}
		System.out.println("Project create other user");
	}
}
