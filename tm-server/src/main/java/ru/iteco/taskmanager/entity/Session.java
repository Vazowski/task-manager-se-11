package ru.iteco.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractEntity {

	private Long timestamp;

    private String userId;

    private String signature;	
}
