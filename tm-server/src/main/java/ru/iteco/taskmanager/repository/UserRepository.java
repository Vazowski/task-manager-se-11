package ru.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.api.repository.IUserRepository;
import ru.iteco.taskmanager.entity.User;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
	
	public void merge(@NotNull final String login, @NotNull final String passwordHash, @NotNull final RoleType roleType, @NotNull final String uuid) {
		map.put(uuid, new User(login, passwordHash, roleType, uuid));
	}
	
	@NotNull
	public String getPasswordHash(@NotNull final String uuid) {
		return map.get(uuid).getPasswordHash();
	}
	
	@Nullable
	public User get(@NotNull final String login) {
		for (final User user : map.values()) {
			if (user.getLogin().equals(login))
				return user;
		}
		return null;
	}
}
