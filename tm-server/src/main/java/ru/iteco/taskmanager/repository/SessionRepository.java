package ru.iteco.taskmanager.repository;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.repository.ISessionRepository;
import ru.iteco.taskmanager.entity.Session;

@NoArgsConstructor
public class SessionRepository implements ISessionRepository{

	@Nullable
	private List<Session> list = new ArrayList<>(); 
	
	public void put(Session session) {
		list.add(session);
	}
	
	@Nullable
	public Session get(Session session) {
		if (list.contains(session)) return session;
		return null;
	}
	
	@Nullable
	public Session remove(Session session) {
		if (list.contains(session)) {
			list.remove(session);
			return session;
		}
		return null;
	}
}
