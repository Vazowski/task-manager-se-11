package ru.iteco.taskmanager.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.entity.Session;

@WebService
public interface ISessionEndpoint {

	@WebMethod
	@Nullable
    Session getSession(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "pass") @Nullable final String pass
            );
	
	@WebMethod
	@Nullable
	Session findSession(
			@WebParam(name = "session") @Nullable Session session
			);

	@WebMethod
	void putSession(
			@WebParam(name = "session") @Nullable Session session
			);
	
	@WebMethod
	@Nullable
	Session removeSession(
			@WebParam(name = "session") @Nullable Session session
			);
}
