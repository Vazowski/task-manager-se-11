package ru.iteco.taskmanager.api.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Session;

@WebService
public interface IProjectEndpoint {

	@WebMethod
	void projectMerge(
			@WebParam(name = "session") @Nullable Session session,
			@WebParam(name = "name") @Nullable String projectName, 
			@WebParam(name = "description") @Nullable String description, 
			@WebParam(name = "uuid") @Nullable String uuid, 
			@WebParam(name = "ownerId") @Nullable String ownerId,
			@WebParam(name = "dateBegin") @Nullable String dateBegin, 
			@WebParam(name = "dateEnd") @Nullable String dateEnd
			);

	@WebMethod
	void projectSet(
			@WebParam(name = "session") @Nullable Session session,
			@WebParam(name = "projectList") @Nullable List<Project> list
			);

	@WebMethod
	void removeByUuid(
			@WebParam(name = "session") @Nullable Session session,
			@WebParam(name = "uuid") @Nullable String uuid
			);

	@WebMethod
	void removeByUuidAndOwnerId(
			@WebParam(name = "session") @Nullable Session session,
			@WebParam(name = "uuid") @Nullable String uuid, 
			@WebParam(name = "ownerId") @Nullable String ownerId
			);

	@WebMethod
	void removeAll(
			@WebParam(name = "session") @Nullable Session session
			);

	@WebMethod
	@Nullable 
	Project findByUuid(
			@WebParam(name = "session") @Nullable Session session,
			@WebParam(name = "uuid") @Nullable String uuid
			);

	@WebMethod
	@Nullable 
	Project findByUuidAndOwnerId(
			@WebParam(name = "session") @Nullable Session session,
			@WebParam(name = "uuid") @Nullable String uuid, 
			@WebParam(name = "ownerId") @Nullable String ownerId
			);

	@WebMethod
	@Nullable 
	Project findProjectByName(
			@WebParam(name = "session") @Nullable Session session,
			@WebParam(name = "projectName") @Nullable String name
			);

	@WebMethod
	@Nullable 
	Project findProjectByNameAndOwnerId(
			@WebParam(name = "session") @Nullable Session session,
			@WebParam(name = "projectName") @Nullable String name, 
			@WebParam(name = "ownerId") @Nullable String ownerId
			);

	@WebMethod
	@Nullable 
	List<Project> findAllProject(
			@WebParam(name = "session") @Nullable Session session
			);

	@WebMethod
	@Nullable 
	List<Project> findAllByOwnerId(
			@WebParam(name = "session") @Nullable Session session,
			@WebParam(name = "ownerId") @Nullable String ownerId
			);

	@WebMethod
	@Nullable 
	List<Project> findAllProjectByPartName(
			@WebParam(name = "session") @Nullable Session session,
			@WebParam(name = "ownerId") @Nullable String ownerId, 
			@WebParam(name = "partOfName") @Nullable String partOfName
			);

	@WebMethod
	@Nullable
	List<Project> findAllProjectByPartDescription(
			@WebParam(name = "session") @Nullable Session session,
			@WebParam(name = "ownerId") @Nullable String ownerId, 
			@WebParam(name = "partOfDescription") @Nullable String partOfDescription
			);
}
