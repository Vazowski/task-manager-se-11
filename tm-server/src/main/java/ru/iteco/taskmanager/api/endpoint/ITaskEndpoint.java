package ru.iteco.taskmanager.api.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.Task;

@WebService
public interface ITaskEndpoint {

	@WebMethod
	void taskMerge(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "name") @Nullable String name, 
			@WebParam(name = "description") @Nullable String description, 
			@WebParam(name = "uuid") @Nullable String uuid,
			@WebParam(name = "projectUuid") @Nullable String projectUuid, 
			@WebParam(name = "ownerId") @Nullable String ownerId, 
			@WebParam(name = "dateBegin") @Nullable String dateBegin, 
			@WebParam(name = "dateEnd") @Nullable String dateEnd
			);

	@WebMethod
	void taskSet(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "taskList") @Nullable List<Task> list
			);

	@WebMethod
	void remove(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "name") @Nullable String name
			);

	@WebMethod
	void removeAllByProjectUuid(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "uuid") @Nullable String uuid
			);

	@WebMethod
	void removeAll(
			@WebParam(name = "session") @Nullable final Session session
			);

	@WebMethod
	@Nullable 
	Task findByUuid(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "uuid") @Nullable String uuid
			);
	
	@WebMethod
	@Nullable
	Task findByName(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "name") @Nullable String name
			);

	@WebMethod
	@Nullable 
	Task findByNameAndOwnerId(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "name") @Nullable String name, 
			@WebParam(name = "ownerId") @Nullable String ownerId
			);

	@WebMethod
	@Nullable
	List<Task> findAllTask(
			@WebParam(name = "session") @Nullable final Session session
			);

	@WebMethod
	@Nullable
	List<Task> findAllByProjectUuid(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "projectUuid") @Nullable String projectUuid
			);

	@WebMethod
	@Nullable
	List<Task> findAllByProjectUuidAndOwnerId(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "projectUuid") @Nullable String projectUuid, 
			@WebParam(name = "ownerId") @Nullable String ownerId
			);

	@WebMethod
	@Nullable 
	List<Task> findAllByPartName(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "ownerId") @Nullable String ownerId, 
			@WebParam(name = "partOfName") @Nullable String partOfName
			);

	@WebMethod
	@Nullable
	List<Task> findAllByPartDescription(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "ownerId") @Nullable String ownerId, 
			@WebParam(name = "partOfDescription") @Nullable String partOfDescription
			);
}
