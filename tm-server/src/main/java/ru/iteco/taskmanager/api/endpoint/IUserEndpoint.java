package ru.iteco.taskmanager.api.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.enumerate.RoleType;

@WebService
public interface IUserEndpoint {

	@WebMethod
	@Nullable
	User checkPassword(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @Nullable String login, 
			@WebParam(name = "pass") @Nullable String pass
			);

	@WebMethod
	@Nullable
	User get(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @NotNull String login
			);

	@WebMethod
	@Nullable
	User getById(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "uuid") @NotNull String uuid
			);
	
	@WebMethod
	void set(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "userList") @Nullable List<User> list
			);

	@WebMethod
	@Nullable
	User findByLogin(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @NotNull String login
			);

	@WebMethod
	@Nullable
	List<User> findAll(
			@WebParam(name = "session") @Nullable final Session session
			);

	@WebMethod
	void merge(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @NotNull String login, 
			@WebParam(name = "pass") @NotNull String passwordHash, 
			@WebParam(name = "roleType") @NotNull RoleType roleType
			);
	
	@WebMethod
	void mergeDefault(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @NotNull String login, 
			@WebParam(name = "pass") @NotNull String passwordHash
			);

	@WebMethod
	@Nullable
	String getCurrent(
			@WebParam(name = "session") @Nullable final Session session
			);

	@WebMethod
	void setCurrent(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @NotNull String login
			);
}
