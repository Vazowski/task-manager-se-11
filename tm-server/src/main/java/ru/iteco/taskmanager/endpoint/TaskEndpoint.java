package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.util.SignatureUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

	private IServiceLocator serviceLocator;
	
	@WebMethod
	public void taskMerge(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "name") @Nullable String name, 
			@WebParam(name = "description") @Nullable String description, 
			@WebParam(name = "uuid") @Nullable String uuid,
			@WebParam(name = "projectUuid") @Nullable String projectUuid, 
			@WebParam(name = "ownerId") @Nullable String ownerId, 
			@WebParam(name = "dateBegin") @Nullable String dateBegin, 
			@WebParam(name = "dateEnd") @Nullable String dateEnd
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getTaskService().merge(name, description, uuid, projectUuid, ownerId, dateBegin, dateEnd);
	}

	@WebMethod
	public void taskSet(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "taskList") @Nullable List<Task> list
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getTaskService().set(list);
	}

	@WebMethod
	public void remove(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "name") @Nullable String name
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getTaskService().remove(name);
	}

	@WebMethod
	public void removeAllByProjectUuid(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "uuid") @Nullable String uuid
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getTaskService().removeAllByProjectUuid(uuid);
	}

	@WebMethod
	public void removeAll(
			@WebParam(name = "session") @Nullable final Session session
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getTaskService().removeAll();
	}

	@WebMethod
	public @Nullable Task findByUuid(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "uuid") @Nullable String uuid
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getTaskService().findByUuid(uuid);
	}

	@WebMethod
	public @Nullable Task findByName(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "name") @Nullable String name
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getTaskService().findByName(name);
	}

	@WebMethod
	public @Nullable Task findByNameAndOwnerId(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "name") @Nullable String name, 
			@WebParam(name = "ownerId") @Nullable String ownerId
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getTaskService().findByName(name, ownerId);
	}

	@WebMethod
	public @Nullable List<Task> findAllTask(
			@WebParam(name = "session") @Nullable final Session session
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getTaskService().findAll();
	}

	@WebMethod
	public @Nullable List<Task> findAllByProjectUuid(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "projectUuid") @Nullable String projectUuid
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getTaskService().findAll(projectUuid);
	}

	@WebMethod
	public @Nullable List<Task> findAllByProjectUuidAndOwnerId(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "projectUuid") @Nullable String projectUuid, 
			@WebParam(name = "ownerId") @Nullable String ownerId
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getTaskService().findAll(projectUuid, ownerId);
	}

	@WebMethod
	public @Nullable List<Task> findAllByPartName(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "ownerId") @Nullable String ownerId, 
			@WebParam(name = "partOfName") @Nullable String partOfName
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getTaskService().findAllByPartName(ownerId, partOfName);
	}

	@WebMethod
	public @Nullable List<Task> findAllByPartDescription(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "ownerId") @Nullable String ownerId, 
			@WebParam(name = "partOfDescription") @Nullable String partOfDescription
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getTaskService().findAllByPartDescription(ownerId, partOfDescription);
	}
}