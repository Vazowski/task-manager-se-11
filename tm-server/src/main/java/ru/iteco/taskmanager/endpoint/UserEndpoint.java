package ru.iteco.taskmanager.endpoint;

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.util.SignatureUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint{

	private IServiceLocator serviceLocator;

	@WebMethod
	@Nullable
	public User checkPassword(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @Nullable String login, 
			@WebParam(name = "pass") @Nullable String pass
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getUserService().checkPassword(login, pass);
	}

	@WebMethod
	@Nullable
	public User get(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @NotNull String login
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getUserService().get(login);
	}

	@WebMethod
	@Nullable
	public User getById(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "uuid") @NotNull String uuid
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getUserService().getById(uuid);
	}
	
	@WebMethod
	public void set(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "userList") @Nullable List<User> list
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getUserService().set(list);
	}

	@WebMethod
	@Nullable
	public User findByLogin(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @NotNull String login
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getUserService().findByLogin(login);
	}

	@WebMethod
	@Nullable
	public List<User> findAll(
			@WebParam(name = "session") @Nullable final Session session
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getUserService().findAll();
	}

	@WebMethod
	public void merge(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @NotNull String login, 
			@WebParam(name = "pass") @NotNull String passwordHash, 
			@WebParam(name = "roleType") @NotNull RoleType roleType
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getUserService().merge(login, passwordHash, roleType);
	}

	@WebMethod
	public void mergeDefault(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @NotNull String login, 
			@WebParam(name = "pass") @NotNull String passwordHash
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getUserService().merge(login, passwordHash);
	}

	@WebMethod
	@Nullable
	public String getCurrent(
			@WebParam(name = "session") @Nullable final Session session
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getUserService().getCurrent();
	}

	@WebMethod
	public void setCurrent(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "login") @NotNull String login
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getUserService().setCurrent(login);
	}
}
