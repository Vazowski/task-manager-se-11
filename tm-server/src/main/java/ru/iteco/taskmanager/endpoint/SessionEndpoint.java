package ru.iteco.taskmanager.endpoint;

import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.util.SignatureUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint{

	@NotNull
	private IServiceLocator serviceLocator;

	@WebMethod
	@Nullable
    public Session getSession(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "pass") @Nullable final String pass
            ) {
        @NotNull final Session session = new Session();
        @Nullable final User user = serviceLocator.getUserService().checkPassword(login, pass);
        if (user == null) return null;
        @NotNull final String userId = user.getUuid();
        session.setUserId(userId);
        @NotNull final Long timeStamp =  new Date().getTime();
        session.setTimestamp(timeStamp);
        session.setSignature(SignatureUtil.sign(session));
        return session;
    }
	
	@WebMethod
	public @Nullable Session findSession(
			@WebParam(name = "session") @Nullable final Session session
			) {
		return serviceLocator.getSessionService().get(session);
	}

	@WebMethod
	public void putSession(
			@WebParam(name = "session") @Nullable final Session session
			) {
		serviceLocator.getSessionService().put(session);
	}
	
	@WebMethod
	@Nullable
	public Session removeSession(
			@WebParam(name = "session") @Nullable final Session session
			) {
		return serviceLocator.getSessionService().remove(session);
	}
}
