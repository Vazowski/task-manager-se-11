package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.entity.Domain;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.enumerate.RoleType;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.IDomainEndpoint")
public class DomainEndpoint implements IDomainEndpoint {

	IServiceLocator serviceLocator;

	@WebMethod
	public Domain saveData(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception {
		@NotNull
		final String currentUser = session.getUserId();
		@NotNull 
		final User user = serviceLocator.getUserService().getById(currentUser);
		if (user == null) return null;
        if (!user.getRoleType().equals(RoleType.ADMIN)) return null;

        @Nullable
		Domain domain = makeDomain();
		return serviceLocator.getDomainService().saveData(domain);
    }
	
	@WebMethod
	@Nullable
	public Domain loadData(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception {
		@NotNull
		final String currentUser = session.getUserId();
		@NotNull 
		final User user = serviceLocator.getUserService().getById(currentUser);
		if (user == null) return null;
        if (!user.getRoleType().equals(RoleType.ADMIN)) return null;
        
        return serviceLocator.getDomainService().loadData();
	}
	
	@WebMethod
	@Nullable
	public Domain jaxbSaveXml(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception {
		@NotNull
		final String currentUser = session.getUserId();
		@NotNull 
		final User user = serviceLocator.getUserService().getById(currentUser);
		if (user == null) return null;
        if (!user.getRoleType().equals(RoleType.ADMIN)) return null;
        
        @Nullable
		Domain domain = makeDomain();
		return serviceLocator.getDomainService().jaxbSaveXml(domain);
	}

	@WebMethod
	@Nullable
	public Domain jaxbLoadXml(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception {
		@NotNull
		final String currentUser = session.getUserId();
		@NotNull 
		final User user = serviceLocator.getUserService().getById(currentUser);
		if (user == null) return null;
        if (!user.getRoleType().equals(RoleType.ADMIN)) return null;
        
		return serviceLocator.getDomainService().jaxbLoadXml();
	}

	@WebMethod
	@Nullable
	public Domain jaxbSaveJson(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception {
		@NotNull
		final String currentUser = session.getUserId();
		@NotNull 
		final User user = serviceLocator.getUserService().getById(currentUser);
		if (user == null) return null;
        if (!user.getRoleType().equals(RoleType.ADMIN)) return null;
        
        @Nullable
		Domain domain = makeDomain();
		return serviceLocator.getDomainService().jaxbSaveJson(domain);
	}

	@WebMethod
	@Nullable
	public Domain jaxbLoadJson(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception {
		@NotNull
		final String currentUser = session.getUserId();
		@NotNull 
		final User user = serviceLocator.getUserService().getById(currentUser);
		if (user == null) return null;
        if (!user.getRoleType().equals(RoleType.ADMIN)) return null;
        
		return serviceLocator.getDomainService().jaxbLoadJson();
	}

	@WebMethod
	public Domain jacksonSaveXml(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception {
		@NotNull
		final String currentUser = session.getUserId();
		@NotNull 
		final User user = serviceLocator.getUserService().getById(currentUser);
		if (user == null) return null;
        if (!user.getRoleType().equals(RoleType.ADMIN)) return null;
        
        @Nullable
		Domain domain = makeDomain();
		return serviceLocator.getDomainService().jacksonSaveXml(domain);
	}

	@WebMethod
	@Nullable
	public Domain jacksonLoadXml(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception {
		@NotNull
		final String currentUser = session.getUserId();
		@NotNull 
		final User user = serviceLocator.getUserService().getById(currentUser);
		if (user == null) return null;
        if (!user.getRoleType().equals(RoleType.ADMIN)) return null;
        
		return serviceLocator.getDomainService().jacksonLoadXml();
	}

	@WebMethod
	public Domain jacksonSaveJson(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception {
		@NotNull
		final String currentUser = session.getUserId();
		@NotNull 
		final User user = serviceLocator.getUserService().getById(currentUser);
		if (user == null) return null;
        if (!user.getRoleType().equals(RoleType.ADMIN)) return null;
        
        @Nullable
		Domain domain = makeDomain();
		return serviceLocator.getDomainService().jacksonSaveJson(domain);
	}

	@WebMethod
	@Nullable
	public Domain jacksonLoadJson(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception {
		@NotNull
		final String currentUser = session.getUserId();
		@NotNull 
		final User user = serviceLocator.getUserService().getById(currentUser);
		if (user == null) return null;
        if (!user.getRoleType().equals(RoleType.ADMIN)) return null;
        
		return serviceLocator.getDomainService().jacksonLoadJson();
	}
	
	private Domain makeDomain() {
		@NotNull
		Domain domain = new Domain();
		@Nullable
        final List<User> userList = serviceLocator.getUserService().findAll();
		if (userList != null) domain.setUserList(userList);
        @Nullable
        final List<Project> projectList = serviceLocator.getProjectService().findAll();
        if (projectList != null) domain.setProjectList(projectList);
        @Nullable
        final List<Task> taskList = serviceLocator.getTaskService().findAll();
        if (taskList != null) domain.setTaskList(taskList);
        
        return domain;
	}
}
