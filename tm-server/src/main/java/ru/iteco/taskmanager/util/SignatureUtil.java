package ru.iteco.taskmanager.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ru.iteco.taskmanager.entity.Session;

public class SignatureUtil {

	private static final String salt = "MEGA-SALT";
    private static final Integer cycle = 228;

    @Nullable
    public static String sign(@Nullable final Object value) {
        try {
            @NotNull
            final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull
            final String json = objectMapper.writeValueAsString(value);
            return sign(json);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    private static String sign(@Nullable final String value) {
        if (value == null) return null;
        @Nullable
        String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.getHash(salt + result + salt);
        }
        return result;
    }

    @Nullable
    public static Session validate(@Nullable final Session session) {
        if (session == null) return null;
        String originalSignature = session.getSignature();
        session.setSignature(null);
        session.setSignature(sign(session));
        if (session.getSignature().equals(originalSignature)) return session;
        return null;
    }
}
