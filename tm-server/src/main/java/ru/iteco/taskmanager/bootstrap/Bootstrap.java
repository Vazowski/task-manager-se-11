package ru.iteco.taskmanager.bootstrap;

import javax.xml.ws.Endpoint;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.endpoint.DomainEndpoint;
import ru.iteco.taskmanager.endpoint.ProjectEndpoint;
import ru.iteco.taskmanager.endpoint.SessionEndpoint;
import ru.iteco.taskmanager.endpoint.TaskEndpoint;
import ru.iteco.taskmanager.endpoint.UserEndpoint;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.service.DomainService;
import ru.iteco.taskmanager.service.ProjectService;
import ru.iteco.taskmanager.service.SessionService;
import ru.iteco.taskmanager.service.TaskService;
import ru.iteco.taskmanager.service.UserService;
import ru.iteco.taskmanager.util.HashUtil;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

	private static final String DEFAULT_PASSWORD = "1234";
	@NotNull
	private final ProjectService projectService = new ProjectService(new ProjectRepository(), new TaskRepository());
	@NotNull
	private final TaskService taskService = new TaskService(new TaskRepository());
	@NotNull
	private final UserService userService = new UserService();
	@NotNull
	private final DomainService domainService = new DomainService();
	@NotNull
	private final SessionService sessionService = new SessionService();
	@NotNull
    public static final String USER_URL = "http://localhost:8080/UserWebService?wsdl";
	@NotNull
    public static final String PROJECT_URL = "http://localhost:8080/ProjectWebService?wsdl";
	@NotNull
    public static final String TASK_URL = "http://localhost:8080/TaskWebService?wsdl";
	@NotNull
    public static final String DOMAIN_URL = "http://localhost:8080/DomainWebService?wsdl";
	@NotNull
    public static final String SESSION_URL = "http://localhost:8080/SessionWebService?wsdl";
	
	public void init() {
		try {
            publishEndpoints();
            createDefaultUsers();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	private void createDefaultUsers() {
		userService.merge("root", HashUtil.getHash(DEFAULT_PASSWORD), RoleType.ADMIN);
		userService.merge("guest", HashUtil.getHash(DEFAULT_PASSWORD), RoleType.USER);
	}
	
	private void publishEndpoints() {
		UserEndpoint userEndpoint = new UserEndpoint();
        userEndpoint.setServiceLocator(this);
        Endpoint.publish(USER_URL, userEndpoint);
        
		ProjectEndpoint projectEndpoint = new ProjectEndpoint();
        projectEndpoint.setServiceLocator(this);
        Endpoint.publish(PROJECT_URL, projectEndpoint);

        TaskEndpoint taskEndpoint = new TaskEndpoint();
        taskEndpoint.setServiceLocator(this);
        Endpoint.publish(TASK_URL, taskEndpoint);

        DomainEndpoint domainEndpoint = new DomainEndpoint();
        domainEndpoint.setServiceLocator(this);
        Endpoint.publish(DOMAIN_URL, domainEndpoint);
        
        SessionEndpoint sessionEndpoint = new SessionEndpoint();
        sessionEndpoint.setServiceLocator(this);
        Endpoint.publish(SESSION_URL, sessionEndpoint);
    }
}
