package ru.iteco.taskmanager.service;

import java.util.List;
import java.util.UUID;

import org.eclipse.persistence.sessions.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.repository.UserRepository;
import ru.iteco.taskmanager.util.HashUtil;

public final class UserService extends AbstractService implements IUserService {

	@NotNull
	private UserRepository userRepository;
	@NotNull
	private StringBuilder currentUser = new StringBuilder("root");
	@NotNull
	private Session currentSession;
	
	public UserService() {
		userRepository = new UserRepository();
	}
	
	@Nullable
	public User get(@NotNull String login) {
		return userRepository.get(login);
	}
	
	@Nullable
	public User getById(@NotNull String uuid) {
		return userRepository.findByUuid(uuid);
	}
	
	public void set(@Nullable final List<User> list) {
		if (list == null) return;
		userRepository.clear();
		for (final User user : list) {
			userRepository.merge(user.getLogin(), user.getPasswordHash(), user.getRoleType(), user.getUuid());
		}
	}
	
	@Nullable
	public User findByLogin(@NotNull final String login) {
		@Nullable
		final List<User> users = userRepository.findAll();
		if (users.size() == 0)
			return null;
		for (final User user : users) {
			if (login.equals(user.getLogin()))
				return user;
		}
		return null;
	}
	
	@Nullable
	public List<User> findAll() {
		return userRepository.findAll();
	}
	
	public void merge(@NotNull final String login, @NotNull final String passwordHash, @NotNull final RoleType roleType) {
		@NotNull
		final String uuid = UUID.randomUUID().toString();
		if (!login.equals(null) && !passwordHash.equals(null) && !roleType.equals(null))
			userRepository.merge(login, passwordHash, roleType, uuid);
	}
	
	public void merge(@NotNull final String login, @NotNull final String passwordHash) {
		@NotNull
		final String uuid = UUID.randomUUID().toString();
		if (!login.equals(null) && !passwordHash.equals(null))
			userRepository.merge(login, passwordHash, RoleType.USER, uuid);
	}
	
	@Nullable
	public String getCurrent() {
		return currentUser.toString();
	}
	
	public void setCurrent(@NotNull final String login) {
		if (!login.equals(null))
			this.currentUser = new StringBuilder(login);
	}
	
	@Nullable
    public User checkPassword(@Nullable final String login, @Nullable final String pass) {
        if (login == null || login.isEmpty()) return null;
        if (pass == null || pass.isEmpty()) return null;
        @Nullable final User user = findByLogin(login);
        @Nullable final String passHash = HashUtil.getHash(pass);
        if (user == null || passHash == null) return null;
        if (passHash.equals(user.getPasswordHash())) return user;
        return null;
    }

}
