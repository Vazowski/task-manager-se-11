package ru.iteco.taskmanager.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import ru.iteco.taskmanager.entity.Domain;

public class DomainService {

	@Nullable
	public Domain saveData(@NotNull final Domain domain) throws Exception {
		@NotNull 
        final File file = new File("dump.bin");
        @NotNull 
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull 
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
        return domain;
    }
	
	@Nullable
	public Domain loadData() throws Exception {
		File f = new File("dump.bin");
		if(!f.exists()) {
			System.out.println("File dump.bin not found");
			return null;
		}
		
		@NotNull
		final File file = new File("dump.bin");
        @NotNull
        final FileInputStream fileInputStream = new FileInputStream(file);
        @NotNull
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @Nullable
        final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
		return domain;
	}
	
	@Nullable
	public Domain jaxbSaveXml(@NotNull final Domain domain) throws Exception {
		try {
			@Nullable
			final JAXBContext jaxbContext = JAXBContext.newInstance(domain.getClass());
			@Nullable
			final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(domain, new File("jaxb.xml"));
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
		return domain;
	}
	
	@Nullable
	public Domain jaxbLoadXml() {
		@Nullable
		Domain domain = new Domain();
		try {
			@Nullable
			final JAXBContext jaxbContext = JAXBContext.newInstance(domain.getClass());

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			domain = (Domain) jaxbUnmarshaller.unmarshal(new File("jaxb.xml"));
		  } catch (JAXBException e) {
			e.printStackTrace();
		  }
		return domain;
	}
	
	@Nullable
	public Domain jaxbSaveJson(@NotNull final Domain domain) throws Exception {
		final Map<String, Object> properties = new HashMap<>();
		final Class[] classes = new Class[] {Domain.class};
		System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
		properties.put("eclipselink-media-type", "application/json");
		@Nullable
		final JAXBContext jaxbContext = JAXBContext.newInstance(classes, properties);
		@Nullable
		final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(domain, new File("jaxb.json"));
		return domain;
	}
	
	@Nullable
	public Domain jaxbLoadJson() throws Exception {
		final Map<String, Object> properties = new HashMap<>();
		final Class[] classes = new Class[] {Domain.class};
		System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
		properties.put("eclipselink-media-type", "application/json");
		@Nullable
		final JAXBContext jaxbContext = JAXBContext.newInstance(classes, properties);
		@Nullable
		final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return (Domain) jaxbUnmarshaller.unmarshal(new File("jaxb.json"));
	}
	
	@Nullable
	public Domain jacksonSaveXml(@NotNull final Domain domain) throws Exception {
		@NotNull
		final ObjectMapper mapper = new XmlMapper();
		@NotNull
		final String result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
		FileWriter writer = new FileWriter("jackson.xml");
		writer.write(result);
		writer.flush();
		writer.close();
		return domain;
	}
	
	@Nullable
	public Domain jacksonLoadXml() throws Exception {
		XmlMapper xmlMapper = new XmlMapper();
        String readContent = new String(Files.readAllBytes(Paths.get("jackson.xml")));
        Domain domain = xmlMapper.readValue(readContent, Domain.class);
        return domain;
	}
	
	@Nullable
	public Domain jacksonSaveJson(@NotNull final Domain domain) throws Exception {
		@NotNull
		final ObjectMapper mapper = new ObjectMapper();
		mapper.writerWithDefaultPrettyPrinter().writeValue(new File("jackson.json"), domain);
		return domain;
	}
	
	@Nullable
	public Domain jacksonLoadJson() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		Domain domain = mapper.readValue(new File("jackson.json"), Domain.class);
		return domain;
	}
}
