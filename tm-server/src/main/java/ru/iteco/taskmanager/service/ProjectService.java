package ru.iteco.taskmanager.service;

import java.util.Iterator;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.service.IProjectService;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;

public final class ProjectService extends AbstractService implements IProjectService {
	
	@NotNull
	private ProjectRepository projectRepository;
	@NotNull
	private TaskRepository taskRepository;
	
	public ProjectService(@NotNull final ProjectRepository projectRepository, @NotNull final TaskRepository taskRepository) {
		this.projectRepository = projectRepository;
		this.taskRepository = taskRepository;
	}
	
	public void merge(@NotNull final String name, @NotNull final String description, @NotNull final String uuid, @NotNull final String ownerId, @NotNull final String dateBegin, @NotNull final String dateEnd) {
		if (!name.equals(null) && !description.equals(null) && !uuid.equals(null) && !ownerId.equals(null) && !dateBegin.equals(null) && !dateEnd.equals(null))
			projectRepository.merge(name, description, uuid, ownerId, dateBegin, dateEnd);
	}

	public void set(@Nullable final List<Project> list) {
		if (list == null) return;
		projectRepository.clear();
		for (final Project project : list) {
			projectRepository.merge(project.getName(), project.getDescription(), project.getUuid(), project.getOwnerId(), project.getDateBegin(), project.getDateEnd());
		}
	}
	
	public void remove(@NotNull final String uuid) {
		if (!uuid.equals(null)) {
			taskRepository.remove(uuid);
			projectRepository.remove(uuid);
		}
	}
	
	public void remove(@NotNull final String uuid, @NotNull final String ownerId) {
		if (!uuid.equals(null) && !uuid.equals(ownerId)) {
			taskRepository.remove(uuid, ownerId);
			projectRepository.remove(uuid, ownerId);
		}
	}
	
	public void removeAll() {
		taskRepository.clear();
		projectRepository.clear();
	}
	
	@Nullable
	public Project findByUuid(@NotNull final String uuid) {
		if (!uuid.equals(null)) {
			return projectRepository.findByUuid(uuid);
		}
		return null;
	}
	
	@Nullable
	public Project findByUuid(@NotNull final String uuid, @NotNull final String ownerId) {
		if (!uuid.equals(null) && !ownerId.equals(null)) {
			return projectRepository.findByUuid(uuid, ownerId);
		}
		return null;
	}
	
	@Nullable
	public Project findByName(@NotNull final String name) {
		if (!name.equals(null)) {
			return projectRepository.findByName(name);
		}
		return null;
	}
	
	@Nullable
	public Project findByName(@NotNull final String name, @NotNull final String ownerId) {
		if (!name.equals(null) && !ownerId.equals(null)) {
		return projectRepository.findByName(name, ownerId);
		}
		return null;
	}
	
	@Nullable
	public List<Project> findAll() {
		List<Project> list = projectRepository.findAll();
		if (list.size() > 0)
			return list;
		return null;
	}
	
	@Nullable
	public List<Project> findAll(@NotNull final String ownerId) {
		if (!ownerId.equals(null)) {
			final List<Project> list = projectRepository.findAll(ownerId);
			if (list.size() > 0)
				return list;
		}
		return null;
	}
	
	@Nullable
	public List<Project> findAllByPartName(@NotNull final String ownerId, @NotNull final String partOfName) {
		if (!ownerId.equals(null) || !partOfName.equals(null)) {
			List<Project> list = projectRepository.findAll(ownerId);
			final Iterator<Project> iterator = list.iterator();
			while (iterator.hasNext()) {
				@Nullable 
				final Project project = iterator.next();
				if (!project.getName().contains(partOfName)) {
					iterator.remove();
				}
			}
			return list;
		}
		return null;
	}
	
	@Nullable
	public List<Project> findAllByPartDescription(@NotNull final String ownerId, @NotNull final String partOfDescription) {
		if (!ownerId.equals(null) || !partOfDescription.equals(null)) {
			List<Project> list = projectRepository.findAll(ownerId);
			final Iterator<Project> iterator = list.iterator();
			while (iterator.hasNext()) {
				@Nullable 
				final Project project = iterator.next();
				if (!project.getDescription().contains(partOfDescription)) {
					iterator.remove();
				}
			}
			return list;
		}
		return null;
	}
}
